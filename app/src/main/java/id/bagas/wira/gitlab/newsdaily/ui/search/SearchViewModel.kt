package id.bagas.wira.gitlab.newsdaily.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bagas.wira.gitlab.newsdaily.data.NewsRepository
import id.bagas.wira.gitlab.newsdaily.models.Article
import id.bagas.wira.gitlab.newsdaily.utils.onError
import id.bagas.wira.gitlab.newsdaily.utils.onException
import id.bagas.wira.gitlab.newsdaily.utils.onSuccess
import kotlinx.coroutines.launch

class SearchViewModel(
    private val repository: NewsRepository
) : ViewModel() {

    private val newsArticlesLiveData = MutableLiveData<List<Article>>()
    val newsArticle: LiveData<List<Article>> get() = newsArticlesLiveData

    fun search(text: String, page: Int) {
        viewModelScope.launch {
            repository.searchNews(text, page).onSuccess {
                newsArticlesLiveData.postValue(it.articles)
            }.onError { code, message ->

            }.onException {

            }
        }
    }
}