package id.bagas.wira.gitlab.newsdaily.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Cache
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import id.bagas.wira.gitlab.newsdaily.BuildConfig
import id.bagas.wira.gitlab.newsdaily.data.ApiService
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    //GENERATE CACHE SINGLETON
    single { return@single Cache(androidContext().cacheDir, 10 * 1024 * 1024) }

    //GENERATE OKHTTP CLIENT SINGLETON
    single { return@single OkHttpClient.Builder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .cache(get<Cache>())
        .addInterceptor{
                chain ->
            val newRequest = chain.request().newBuilder()
                .header("X-API-Key", BuildConfig.NEWS_API_KEY)
            chain.proceed(newRequest.build())
        }
        .build()
    }

    //GENERATE RETROFIT CLIENT SINGLETON
    single { return@single Retrofit.Builder()
        .baseUrl(BuildConfig.API_BASE_URL)
        .client(get<OkHttpClient>())
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
        .build()
    }

    //GENERATE API SERVICE SINGLETON
    single { return@single get<Retrofit>().create(ApiService::class.java) }
}