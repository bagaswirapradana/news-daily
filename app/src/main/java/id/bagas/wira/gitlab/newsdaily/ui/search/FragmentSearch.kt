package id.bagas.wira.gitlab.newsdaily.ui.search

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.bagas.wira.gitlab.newsdaily.base.BaseFragment
import id.bagas.wira.gitlab.newsdaily.databinding.FragmentSearchBinding
import id.bagas.wira.gitlab.newsdaily.models.Article
import id.bagas.wira.gitlab.newsdaily.ui.home.NewsAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragmentSearch : BaseFragment<FragmentSearchBinding>() {

    private val recylerviewAdapter by lazy {
        NewsAdapter { article ->
            openWebView(article)
        }
    }

    private val linearLayoutManager by lazy { LinearLayoutManager(requireContext()) }

    private val viewModel by viewModel<SearchViewModel>()

    private var loading = true
    private var keyWord = ""
    private var pastVisiblesItems = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0
    private var currentPage = 1

    override fun setBindingView(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentSearchBinding.inflate(layoutInflater, container, false)

    override fun main() {
        binding.recylerviewArticlesResult.apply {
            layoutManager = linearLayoutManager
            adapter = recylerviewAdapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0) { //check for scroll down
                        visibleItemCount = linearLayoutManager.childCount
                        totalItemCount = linearLayoutManager.itemCount
                        pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition()
                        if (loading) {
                            if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                                loading = false
                                viewModel.search(keyWord, currentPage)
                                loading = true
                            }
                        }
                    }
                }
            })
        }
        binding.etSearch.doAfterTextChanged {
            if (it?.isNotEmpty() == true) {
                binding.ivSearchClear.visibility = View.VISIBLE
                keyWord = it.toString()
                viewModel.search(keyWord, currentPage)
            } else binding.ivSearchClear.visibility = View.GONE
        }
        binding.ivSearchClear.setOnClickListener {
            keyWord = ""
            binding.etSearch.text?.clear()
        }

        viewModel.newsArticle.observe(viewLifecycleOwner) {
            recylerviewAdapter.submitList(it.toMutableList())
        }
    }

    private fun openWebView(article: Article) {
        val builder = CustomTabsIntent.Builder().apply {
            setShowTitle(true)
            setStartAnimations(requireContext(), android.R.anim.fade_in, android.R.anim.fade_out)
            setExitAnimations(requireContext(), android.R.anim.fade_in, android.R.anim.fade_out)
        }
        builder.build().apply {
            intent.setPackage("com.android.chrome")
            launchUrl(requireContext(), Uri.parse(article.url))
        }
    }

    override fun onDestroyView() {
        keyWord = ""
        super.onDestroyView()
    }
}