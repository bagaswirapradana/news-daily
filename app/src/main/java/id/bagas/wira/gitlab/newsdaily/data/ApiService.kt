package id.bagas.wira.gitlab.newsdaily.data

import id.bagas.wira.gitlab.newsdaily.data.response.NewsResponse
import id.bagas.wira.gitlab.newsdaily.data.response.SourceResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/v2/top-headlines")
    suspend fun loadCategoryHeadlines(
        @Query("sources") source: String,
    ): Response<NewsResponse>

    @GET("/v2/sources")
    suspend fun getSourceBased(@Query("category") category: String): Response<SourceResponse>

    @GET("/v2/everything")
    suspend fun searchNews(
        @Query("q") keyword: String,
        @Query("sortBy") sortBy: String,
        @Query("page") page: Int
    ): Response<NewsResponse>
}