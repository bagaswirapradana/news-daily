package id.bagas.wira.gitlab.newsdaily

import androidx.multidex.MultiDexApplication
import timber.log.Timber

class NewsDailyApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}