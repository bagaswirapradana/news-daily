package id.bagas.wira.gitlab.newsdaily.models

import com.google.gson.annotations.SerializedName
import java.util.Date

data class Article(
    @SerializedName("author")
    var author: String?,
    @SerializedName("content")
    var content: String?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("publishedAt")
    var publishedAt: Date,
    @SerializedName("title")
    var title: String?,
    @SerializedName("url")
    var url: String?,
    @SerializedName("urlToImage")
    var urlToImage: String?,
    var isBookmark: Boolean = false
)