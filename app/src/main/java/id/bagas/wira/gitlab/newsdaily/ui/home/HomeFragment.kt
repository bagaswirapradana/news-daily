package id.bagas.wira.gitlab.newsdaily.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.android.material.tabs.TabLayoutMediator
import id.bagas.wira.gitlab.newsdaily.base.BaseFragment
import id.bagas.wira.gitlab.newsdaily.databinding.FragmentHomeBinding

class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private val newsCategoryList by lazy {
        listOf("business", "entertainment", "technology", "health", "science", "sports", "general")
    }

    override fun setBindingView(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentHomeBinding.inflate(inflater, container, false)

    override fun main() {
        val adapter = CategoriesPagerAdapter(this@HomeFragment)
        for (category in newsCategoryList) {
            adapter.addFragment(NewsCategoriesFragment.getInstance(category), category)
        }

        with(binding) {
            viewPager.adapter = adapter
            viewPager.offscreenPageLimit = 4
            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = adapter.getTabTitle(position)
            }.attach()
        }
    }
}