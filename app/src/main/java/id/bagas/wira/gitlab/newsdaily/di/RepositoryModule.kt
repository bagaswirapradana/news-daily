package id.bagas.wira.gitlab.newsdaily.di

import id.bagas.wira.gitlab.newsdaily.data.NewsRepository
import id.bagas.wira.gitlab.newsdaily.data.NewsRepositoryImp
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.bind
import org.koin.dsl.module

val repositoryModule = module {
    factoryOf(::NewsRepositoryImp) bind NewsRepository::class
}