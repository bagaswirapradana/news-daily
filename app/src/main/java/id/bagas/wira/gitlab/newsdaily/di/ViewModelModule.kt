package id.bagas.wira.gitlab.newsdaily.di

import id.bagas.wira.gitlab.newsdaily.ui.home.NewsCategoriesViewModel
import id.bagas.wira.gitlab.newsdaily.ui.search.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val viewModelModule = module {
    viewModelOf(::NewsCategoriesViewModel)
    viewModelOf(::SearchViewModel)
}