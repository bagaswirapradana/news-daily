package id.bagas.wira.gitlab.newsdaily.ui.activity

import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import id.bagas.wira.gitlab.newsdaily.R
import id.bagas.wira.gitlab.newsdaily.base.BaseActivity
import id.bagas.wira.gitlab.newsdaily.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val navHostFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
    }
    private val navController by lazy { navHostFragment.navController }

    override fun setBindingView() = ActivityMainBinding.inflate(layoutInflater)

    override fun initView() {
        binding?.bottomNavBar?.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp() = navController.navigateUp() || super.onSupportNavigateUp()

}