package id.bagas.wira.gitlab.newsdaily.data

import id.bagas.wira.gitlab.newsdaily.utils.handleApi

class NewsRepositoryImp(
    private val apiService: ApiService
) : NewsRepository {
    override suspend fun getCategorizedNewsHeadLines(source: String) =
        handleApi { apiService.loadCategoryHeadlines(source) }

    override suspend fun getSourcesNews(category: String) = handleApi { apiService.getSourceBased(category) }

    override suspend fun searchNews(text: String, page: Int) =
        handleApi { apiService.searchNews(keyword = text, sortBy = "relevancy", page = page) }
}