package id.bagas.wira.gitlab.newsdaily.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {

    protected var binding: VB? = null
        private set

    protected abstract fun setBindingView(): VB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setBindingView()
        setContentView(binding?.root)
        initView()
    }

    protected abstract fun initView()
}