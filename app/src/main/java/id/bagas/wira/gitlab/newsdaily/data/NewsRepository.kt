package id.bagas.wira.gitlab.newsdaily.data

import id.bagas.wira.gitlab.newsdaily.data.response.NewsResponse
import id.bagas.wira.gitlab.newsdaily.data.response.SourceResponse

interface NewsRepository {
    suspend fun getCategorizedNewsHeadLines(source: String): NetworkResult<NewsResponse>

    suspend fun getSourcesNews(category: String): NetworkResult<SourceResponse>

    suspend fun searchNews(text: String, page: Int): NetworkResult<NewsResponse>
}