package id.bagas.wira.gitlab.newsdaily.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bagas.wira.gitlab.newsdaily.data.NewsRepository
import id.bagas.wira.gitlab.newsdaily.models.Article
import id.bagas.wira.gitlab.newsdaily.models.Source
import id.bagas.wira.gitlab.newsdaily.utils.onError
import id.bagas.wira.gitlab.newsdaily.utils.onException
import id.bagas.wira.gitlab.newsdaily.utils.onSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class NewsCategoriesViewModel(
    private val repository: NewsRepository
) : ViewModel() {

    private val newsSourceLiveData = MutableLiveData<List<Source>>()
    val newsSource: LiveData<List<Source>> get() = newsSourceLiveData

    private val newsArticlesLiveData = MutableLiveData<List<Article>>()
    val newsArticle: LiveData<List<Article>> get() = newsArticlesLiveData

    fun getSources(category: String) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getSourcesNews(category).onSuccess {
                newsSourceLiveData.postValue(it.source)
            }.onError { code, message ->
                Timber.d("AAA $message")
            }.onException {
                Timber.d("AAA ${it.message}")
            }
        }
    }

    fun getArticles(source: String) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getCategorizedNewsHeadLines(source).onSuccess {
                newsArticlesLiveData.postValue(it.articles)
            }.onError { code, message ->
                Timber.d("AAA $code $message")
            }.onException {
                Timber.d("AAA ${it.message}")
            }
        }
    }
}