package id.bagas.wira.gitlab.newsdaily.data.response

import com.google.gson.annotations.SerializedName
import id.bagas.wira.gitlab.newsdaily.models.Article

data class NewsResponse(
    @SerializedName("articles")
    var articles: List<Article> = emptyList(),
    @SerializedName("status")
    var status: String?,
    @SerializedName("totalResults")
    var totalResults: Int?
)
