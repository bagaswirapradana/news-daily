package id.bagas.wira.gitlab.newsdaily.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import id.bagas.wira.gitlab.newsdaily.databinding.ItemNewsBinding
import id.bagas.wira.gitlab.newsdaily.models.Article

class NewsAdapter(
    private val onArticleClicked: (Article) -> Unit
) : ListAdapter<Article, NewsAdapter.ArticleViewHolder>(ArticleDiffCallback())  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ArticleViewHolder(ItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ArticleViewHolder(private val binder: ItemNewsBinding) : RecyclerView.ViewHolder(binder.root) {
        fun bind(article: Article) {
            binder.article = article
            binder.root.setOnClickListener { onArticleClicked(article) }
            binder.executePendingBindings()
        }
    }

    private class ArticleDiffCallback : DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean =
            oldItem == newItem
    }
}
