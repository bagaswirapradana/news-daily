package id.bagas.wira.gitlab.newsdaily.utils

import android.annotation.SuppressLint
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import id.bagas.wira.gitlab.newsdaily.R
import java.text.SimpleDateFormat
import java.util.Date

@BindingAdapter("coverImage")
fun loadImage(imageView: ImageView, imageURL: String?) {
    Glide.with(imageView.context)
        .load(imageURL)
        .centerCrop()
        .placeholder(R.color.iconColorDefault)
        .error(android.R.color.black)
        .into(imageView)
}

@SuppressLint("SimpleDateFormat")
@BindingAdapter("publishedAt")
fun setPublishedAt(view: TextView, date: Date) {
    view.text =  SimpleDateFormat("HH:mm 'on' MMM dd, yyyy").format(date)
}