package id.bagas.wira.gitlab.newsdaily.initializers

import android.content.Context
import androidx.startup.Initializer
import id.bagas.wira.gitlab.newsdaily.di.networkModule
import id.bagas.wira.gitlab.newsdaily.di.repositoryModule
import id.bagas.wira.gitlab.newsdaily.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin

class KoinInitializers : Initializer<KoinApplication> {
    override fun create(context: Context): KoinApplication {
       return startKoin {
            androidContext(context)
            modules(
                networkModule,
                repositoryModule,
                viewModelModule
            )
        }
    }

    override fun dependencies(): List<Class<out Initializer<*>>> = emptyList()
}