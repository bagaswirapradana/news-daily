package id.bagas.wira.gitlab.newsdaily.data.response

import com.google.gson.annotations.SerializedName
import id.bagas.wira.gitlab.newsdaily.models.Source

data class SourceResponse(
    @SerializedName("status")
    val status: String,
    @SerializedName("sources")
    val source: List<Source>,
)
