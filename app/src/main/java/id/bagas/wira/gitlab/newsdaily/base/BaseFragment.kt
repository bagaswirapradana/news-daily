package id.bagas.wira.gitlab.newsdaily.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<VB : ViewBinding> : Fragment() {

    protected abstract fun setBindingView(inflater: LayoutInflater, container: ViewGroup?): VB

    private var _binding: VB? = null
    protected val binding get() = _binding!!

    abstract fun main()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = setBindingView(inflater, container)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        main()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}