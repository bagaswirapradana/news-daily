package id.bagas.wira.gitlab.newsdaily.ui.home

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.browser.customtabs.CustomTabsIntent
import androidx.recyclerview.widget.LinearLayoutManager
import id.bagas.wira.gitlab.newsdaily.base.BaseFragment
import id.bagas.wira.gitlab.newsdaily.databinding.FragmentNewsCategoriesBinding
import id.bagas.wira.gitlab.newsdaily.models.Article
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewsCategoriesFragment : BaseFragment<FragmentNewsCategoriesBinding>() {

    private val viewModel by viewModel<NewsCategoriesViewModel>()
    private val recylerviewAdapter by lazy {
        NewsAdapter { article ->
            openWebView(article)
        }
    }

    private var newsCategory: String? = null

    companion object {
        fun getInstance(newsCategory: String): NewsCategoriesFragment {
            val fragment = NewsCategoriesFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            fragment.newsCategory = newsCategory
            return fragment
        }
    }

    override fun setBindingView(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentNewsCategoriesBinding.inflate(inflater, container, false)

    override fun main() {
        binding.recylerviewArticles.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = recylerviewAdapter
        }
        newsCategory?.let {
            viewModel.getSources(it)
        }

        viewModel.newsSource.observe(viewLifecycleOwner) { sources ->
            val spinnerAdapter =
                ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, sources.map {
                    it.name
                })
            binding.spinnerCategories.apply {
                adapter = spinnerAdapter
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        newsCategory?.let { viewModel.getArticles(sources[0].id.orEmpty()) }
                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        viewModel.getArticles(sources[position].id.orEmpty())
                    }
                }
            }
        }

        viewModel.newsArticle.observe(viewLifecycleOwner) {
            recylerviewAdapter.submitList(it.toMutableList())
        }
    }

    private fun openWebView(article: Article) {
        val builder = CustomTabsIntent.Builder().apply {
            setShowTitle(true)
            setStartAnimations(requireContext(), android.R.anim.fade_in, android.R.anim.fade_out)
            setExitAnimations(requireContext(), android.R.anim.fade_in, android.R.anim.fade_out)
        }
        builder.build().apply {
            intent.setPackage("com.android.chrome")
            launchUrl(requireContext(), Uri.parse(article.url))
        }
    }
}